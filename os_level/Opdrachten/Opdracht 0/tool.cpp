#include <iostream>
#include <string>

std::string translate(std::string line, std::string shift)
{
  std::string result = "";
  int intshift{std::stoi(shift)};

  for (uint32_t i{0}; i < line.length(); i++)
  {
    if (!isupper(line[i]))
      result += char(int(line[i] + intshift - 97) % 26 + 97);
    else
      result += char(int(line[i] + intshift - 65) % 26 + 65);
  }
  return result;
}

int main(int argc, char *argv[])
{
  std::string line;

  if (argc != 2)
  {
    std::cerr << "Deze functie heeft exact 1 argument nodig" << std::endl;
    return -1;
  }

  while (std::getline(std::cin, line))
  {
    std::cout << translate(line, argv[1]) << std::endl;
  }

  return 0;
}

// ls | ./tool 2